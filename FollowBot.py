import sys
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by   import By
from selenium.webdriver.support.ui  import WebDriverWait
from selenium.webdriver.support     import expected_conditions as EC
from selenium.common.exceptions     import TimeoutException


#funzione leggi intero file data
def ReadData():
    with open('.\\data.txt', "rU") as f:
        lines = f.readlines()

        return lines


#funzione leggi username da file data
def readusername(i):
    username = text[i].split(" | ")[0]

    return username


#funzione leggi password da file data
def readpassword(i):
    passwd = text[i].split(" | ")[1]

    return passwd


##inizio programma

text         = ReadData()
chromedriver = 'C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe'
driver       = webdriver.Chrome(chromedriver)

driver.get('https://www.instagram.com/accounts/login')

for i in range(int(sys.argv[2]), int(sys.argv[3])):
    username = driver.find_element_by_name("username")
    password = driver.find_element_by_name("password")

    username.send_keys("")
    password.send_keys("")
    # username.send_keys(readusername(i))
    # password.send_keys(readpassword(i))

    driver.find_element_by_class_name('_5f5mN       ').click()

    try:
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'coreSpriteDesktopNavProfile')))
    except TimeoutException:
        i -= 1
        continue

    driver.get('https://www.instagram.com/riccardo.giuriola/')
    # driver.get('https://www.instagram.com/' + sys.argv[4])
    driver.find_element_by_class_name('_5f5mN        ').click()
    time.sleep(0.5)
    driver.get('https://www.instagram.com/chris_broughton28485')
    # driver.get('https://www.instagram.com/' + readusername(i))
    driver.find_element_by_class_name('glyphsSpriteSettings__outline__24__grey_9 ').click()
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'aOOlW   ')))
    logout = driver.find_elements_by_class_name('aOOlW   ')
    logout[4].click()
    driver.get('https://www.instagram.com/accounts/login')

driver.quit()
