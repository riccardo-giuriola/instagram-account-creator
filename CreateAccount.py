# coding=utf-8
import sys
import random
import csv
import time
from selenium                          import webdriver
from selenium.webdriver.common.keys    import Keys
from selenium.webdriver.common.by      import By
from selenium.webdriver.support.ui     import WebDriverWait
from selenium.webdriver.support        import expected_conditions as EC
from selenium.common.exceptions        import TimeoutException
from selenium.webdriver.chrome.options import Options


email         = ["@gmail.com", "@hotmail.com", "@mail.co.uk"]
chars         = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
special_chars = ['.', '_']


#funzione crea nome utente
def makeusername(name, surname):
    toReturn = ''

    if(random.randint(0, 1) == 1):
        toReturn += special_chars[random.randint(0,1)]

    if(random.randint(0, 1) == 1):
        toReturn += name + special_chars[random.randint(0,1)] + surname

        if(random.randint(0, 1) == 1):
            toReturn += str(random.randint(1,1000))
    else:
        toReturn = name + surname

        if(random.randint(0, 1) == 1):
            toReturn += str(random.randint(1,1000))

    # with open('C:\\Users\\meren\\Desktop\\InstagramBot\\data.txt', 'a') as f:
    #     f.write(toReturn.replace('[\'', '').replace('\']', '') + " | ")

    return str(toReturn).replace('[\'', '').replace('\']', '')


#funzione crea email
def makemail(name, surname):
    toReturn = name + '.' + surname + str(random.randint(1,1000)) + email[random.randint(0,2)]
    return str(toReturn).replace('[\'', '').replace('\']', '')


#funzione genera nome
def createname():
    with open('C:\\Users\\meren\\Documents\\InstagramBot\\FirstNames.csv', "rU") as csvfile:
        reader = csv.reader(csvfile)
        for _ in range (random.randint(0,5494)):
            name = next(reader)

    return str(name).replace('[\'', '').replace('\']', '')


#funzione genera cognome
def createsurname():
    with open('C:\\Users\\meren\\Documents\\InstagramBot\\LastNames.csv', "rU") as csvfile:
        reader = csv.reader(csvfile)
        for _ in range (random.randint(1,88799)):
            surname = next(reader)

    return str(surname).replace('[\'', '').replace('\']', '')


#funzione genera password
def createpassword():
    passwd = ''
    for _ in range(15):
        passwd += chars[random.randint(0,35)]

    # with open('C:\\Users\\meren\\Desktop\\InstagramBot\\data.txt', 'a') as f:
    #     f.write(passwd + "\n")

    return passwd


#funzione leggi proxy
def readproxy(i):
    proxy = ''

    with open("C:\\Users\\meren\\Documents\\InstagramBot\\proxies.txt", 'r') as f:
        proxy = f.readlines()

    return str(proxy[i]).replace('[\'', '').replace('\']', '')


#funzione pulisci campo nel form
def clearfield(element):
    element.send_keys(len(element.get_attribute('value')) * Keys.BACKSPACE)


##inizio programma

# import ProxyScraper
chromedriver = 'C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe'
accounts_created, proxy_index = 0, 1

for _ in range(0, int(sys.argv[2])):
    options = Options()
    options.add_argument('--proxy-server=https://' + readproxy(proxy_index))
    options.add_argument('--proxy-server=socks://' + readproxy(proxy_index))
    options.add_argument('--proxy-server=http://' + readproxy(proxy_index))
    driver = webdriver.Chrome(chromedriver, chrome_options = options)
    # driver = webdriver.Chrome(chromedriver)

    # for i in range(0, 5):
    driver.set_page_load_timeout(45)

    try:
        driver.get('https://www.instagram.com/')
    except:
        if(proxy_index >= 499):
            proxy_index = 0
        else:
            proxy_index += 1

        driver.quit()
        continue

    emailOrPhone = driver.find_element_by_name("emailOrPhone")
    fullName     = driver.find_element_by_name("fullName")
    username     = driver.find_element_by_name("username")
    password     = driver.find_element_by_name("password")

    name, surname    = createname(), createsurname()
    full             = name + ' ' + surname
    usrname, passwd = makeusername(name, surname), createpassword()
    time.sleep(2)
    emailOrPhone.send_keys(makemail(name, surname))
    time.sleep(1)
    fullName.send_keys(full)
    time.sleep(1)
    username.send_keys(usrname)
    time.sleep(1)
    password.send_keys(passwd)

    condition = False
    while not condition:
        try:
            WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, "//form[@class='XFYOY']/div[5]/div/div[@class='i24fI']/span[@class='coreSpriteInputAccepted gBp1f']")))
            condition = True
        except:
            print('username già esistente')
            clearfield(username)
            usrname = makeusername(name, surname)
            time.sleep(2)
            username.send_keys(usrname)
            password.click()

    time.sleep(2)
    buttons = driver.find_elements_by_class_name('oF4XW ')
    buttons[1].click()

    try:
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, 'igCoreRadioButtonageRadioabove_18'))).click()
        time.sleep(1)
        driver.find_element_by_class_name('L3NKy ').click()
    except:
        print('none')

    try:
        nav_profile = WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.CLASS_NAME, 'coreSpriteDesktopNavProfile')))
        time.sleep(2)
        nav_profile.click()
    except TimeoutException:
        if(proxy_index >= 499):
            proxy_index = 0
        else:
            proxy_index += 1

        driver.quit()
        continue

    with open("C:\\Users\\meren\\Documents\\InstagramBot\\data.txt", 'a') as f:
        f.write(usrname + " | ")
        f.write(passwd + "\n")

    accounts_created += 1

    driver.find_element_by_class_name('glyphsSpriteSettings__outline__24__grey_9 ').click()

    try:
        WebDriverWait(driver, 40).until(EC.presence_of_element_located((By.CLASS_NAME, 'aOOlW   ')))
    except:
        if(proxy_index >= 499):
            proxy_index = 0
        else:
            proxy_index += 1

        driver.quit()
        continue

    logout = driver.find_elements_by_class_name('aOOlW   ')
    logout[4].click()
    time.sleep(5)

    if(proxy_index >= 499):
        proxy_index = 0
    else:
        proxy_index += 1

    driver.quit()

print (str(accounts_created) + ' accounts were created')
